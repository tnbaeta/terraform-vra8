variable "resourceGroup" {
  type        = string
  description = "Un grupo de recursos es una colección de recursos que comparten los mismos permisos, ciclo de vida y directivas."
  default = "pocvra-resourceGroup"
}

variable "region" {
  type        = string
  description = "La región de Azure en la que debe implementarse el grupo de recursos."
  default     = "eastus2"
}