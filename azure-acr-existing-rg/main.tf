terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.59.0"
    }
  }
}
# Configure the Microsoft Azure Provider
provider "azurerm" {
    # The "feature" block is required for AzureRM provider 2.x. 
    # If you're using version 1.x, the "features" block is not allowed.
    #version = "~>2.0"
    features {}
}

resource "azurerm_container_registry" "vraAcr" {
  name                = var.registryName
  resource_group_name = var.resourceGroup
  location            = var.region
  sku                 = var.SKU
  admin_enabled       = true
}