variable "resourceGroup" {
  type        = string
  description = "Un grupo de recursos es una colección de recursos que comparten los mismos permisos, ciclo de vida y directivas."
  default = "pocvra-resourceGroup"
}

variable "region" {
  type        = string
  description = "La región de Azure en la que debe implementarse el grupo de recursos."
  default     = "eastus2"
}

variable "storageAccountName" {
  type        = string
  description = "Escriba un nombre para la nueva cuenta de almacenamiento."
  default = "pocvra-resourceGroup"
}

variable "accountTier" {
  type        = string
  description = "Elija el nivel de cuenta deseado para la nueva cuenta de almacenamiento."
  default     = "Standard"
}

variable "accountReplicationType" {
  type        = string
  description = "Elija el tipo de replicación de cuenta que desee."
  default     = "LRS"
}

variable "accountKind" {
  type        = string
  description = "Elija el tipo de cuenta que desee."
  default     = "FileStorage"
}
