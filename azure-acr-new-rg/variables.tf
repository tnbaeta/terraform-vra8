variable "resourceGroup" {
  type        = string
  description = "Choose the resource group for the container registry."
  default = "pocvra-resourceGroup"
}

variable "registryName" {
  type        = string
  description = "Choose a name for the container registry."
  default     = "pocvraAcr"
}

variable "region" {
	type				= string
  description = "Choose where on which region the registry container should be created."
  default     = "eastus2"
}

variable "SKU" {
  type        = string
  description = "Choose the SKU Level."
  default     = "Standard"
}