###########################################################################
# Terraform required providers                                            #
###########################################################################
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_availability_zones" "available" {}

###########################################################################

###########################################################################
# IAM Roles and policies for EKS Cluster                #
###########################################################################
resource "aws_iam_role" "eksIamRoleName" {
  name = var.eksIamRoleName

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "amazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eksIamRoleName.name
}

resource "aws_iam_role_policy_attachment" "amazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eksIamRoleName.name
}
###########################################################################

###########################################################################
# IAM Roles and policies for Worker Gropus                                #
###########################################################################
resource "aws_iam_role" "eksNodeIamRole" {
  name = var.eksNodeIamRoleName

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "amazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eksNodeIamRole.name
}

resource "aws_iam_role_policy_attachment" "amazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eksNodeIamRole.name
}

resource "aws_iam_role_policy_attachment" "amazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eksNodeIamRole.name
}
###########################################################################

###########################################################################
# New VPC and subnet settings for EKS cluster                             #
###########################################################################
resource "aws_vpc" "eksVpc" {
  cidr_block           = var.vpcCidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = var.vpcName
  }
}

resource "aws_subnet" "eksSubnet" {
  count = 2

  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.0.${count.index}.0/24"
  vpc_id                  = aws_vpc.eksVpc.id
  map_public_ip_on_launch = true

  tags = map(
    "Name", "subnet0${count.index}",
    "kubernetes.io/cluster/${var.clusterName}", "shared",
  )
}

resource "aws_internet_gateway" "eksInternetGateway" {
  vpc_id = aws_vpc.eksVpc.id

  tags = {
    Name = "pocvra-eksInternetGateway"
  }
}

resource "aws_route_table" "eksRouteTable" {
  vpc_id = aws_vpc.eksVpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.eksInternetGateway.id
  }
}

resource "aws_route_table_association" "eksRouteTableAssociation" {
  count          = 2
  subnet_id      = aws_subnet.eksSubnet.*.id[count.index]
  route_table_id = aws_route_table.eksRouteTable.id
}
###########################################################################

###########################################################################
# New EKS Cluster and settings                                            #
###########################################################################
resource "aws_eks_cluster" "eksCluster" {
  name     = var.clusterName
  role_arn = aws_iam_role.eksIamRoleName.arn
  version  = var.clusterVersion

  vpc_config {
    subnet_ids = aws_subnet.eksSubnet[*].id
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.amazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.amazonEKSVPCResourceController,
  ]
}
###########################################################################

###########################################################################
# New security group for the EKS cluster                                  #
###########################################################################
resource "aws_security_group" "pocvra-eksCluster" {
  name        = var.sgName
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.eksVpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "pocvraClusterIngressHttps" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow any address to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.pocvra-eksCluster.id
  to_port           = 443
  type              = "ingress"
}
###########################################################################

###########################################################################
# New Worker groups settings                                              #
###########################################################################
resource "aws_eks_node_group" "eksNodeGroup" {
  cluster_name    = aws_eks_cluster.eksCluster.name
  node_group_name = var.eksNodeGroupName
  node_role_arn   = aws_iam_role.eksNodeIamRole.arn
  subnet_ids      = aws_subnet.eksSubnet[*].id

  scaling_config {
    desired_size = 1
    max_size     = 3
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.amazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.amazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.amazonEC2ContainerRegistryReadOnly,
  ]
}
