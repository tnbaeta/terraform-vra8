variable "region" {
  type        = string
  description = "Select the region."
  default     = "us-east-1"
}

variable "eksIamRoleName" {
  type        = string
  description = "Choose the nam for the new IAM role for EKS"
  default     = "pocvra-eks-iam-role"
}

variable "eksNodeIamRoleName" {
  type        = string
  description = "Choose the name for the new IAM role for the EKS worker groups"
  default     = "pocvra-eks-node-iam-role"
}

variable "clusterName" {
  type        = string
  description = "Choose a name for the EKS cluster."
  default     = "pocvra-eks-cluster"
}

variable "vpcName" {
  type        = string
  description = "Choose a name for the new VPC."
  default     = "pocvra-eks-vpc"
}

variable "vpcCidr" {
  type        = string
  description = "Choose a CIDR for the VPC."
  default     = "10.0.0.0/16"
}

variable "sgName" {
  type        = string
  description = "Enter a Security Group name for cluster communication with worker nodes"
  default     = "pocvra-eks-sg"
}

variable "clusterVersion" {
  type        = string
  description = "Choose a Kubernetes version for the EKS cluster."
  default     = "1.19"
}

variable "eksNodeGroupName" {
  type        = string
  description = "Enter a name for the cluster node group."
  default     = "pocvra-eks-node-group"
}
