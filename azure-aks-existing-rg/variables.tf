variable "resourceGroup" {
  type        = string
  description = "Un grupo de recursos es una colección de recursos que comparten los mismos permisos, ciclo de vida y directivas."
  default = "pocvra-resourceGroup"
}

variable "clusterName" {
  type        = string
  description = "El nombre del clúster de Azure Kubernetes Service."
  default     = "pocvra-aks-01"
}

variable "region" {
	type				= string
  description = "La región de Azure en la que debe implementarse el clúster."
  default     = "eastus2"
}

variable "dnsPrefix" {
  type        = string
  description = "DNS name prefix to use with the hosted Kubernetes API server FQDN. You will use this to connect to the Kubernetes API when managing containers after creating the cluster."
  default = "pocvra"
}

variable "kubernetesVersion" {
  type        = string
  description = "La versión de Kubernetes que se debe usar para este clúster. Esta versión puede actualizarse después de crear el clúster."
  default = "1.19.9"
}

variable "nodeSize" {
  type        = string
  description = "El tamaño de las máquinas virtuales que formarán los nodos del clúster. Este valor no puede cambiarse después de crear el clúster."
  default = "Standard_D4s_v3"
}

variable "nodeCount" {
  type        = number
  description = "El número de nodos que se deben crear junto con el clúster. El tamaño del clúster puede cambiarse más adelante."
  default = 3
}