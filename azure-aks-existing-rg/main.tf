terraform {
  required_providers {
    azurerm           = {
      source          = "hashicorp/azurerm"
      version         = "=2.59.0"
    }
  }
}
# Configure the Microsoft Azure Provider
provider "azurerm" {
    # The "feature" block is required for AzureRM provider 2.x. 
    # If you're using version 1.x, the "features" block is not allowed.
    #version = "~>2.0"
    features {}
}

resource "azurerm_kubernetes_cluster" "vraAks" {
  name                = var.clusterName
  location            = var.region
  resource_group_name = var.resourceGroup
  dns_prefix          = var.dnsPrefix
  kubernetes_version  = var.kubernetesVersion

  default_node_pool {
    name              = "default"
    node_count        = var.nodeCount
    vm_size           = var.nodeSize
  }

  identity {
    type              = "SystemAssigned"
  }

}

output "kube_config" {
  value               = azurerm_kubernetes_cluster.vraAks.kube_config_raw
}